from aiohttp import web
from urls import setup_routes
from settings import config
from on_startapp import init_db, prepare_data


app = web.Application()
setup_routes(app)
app['config'] = config
app.on_startup.append(init_db)
app.on_startup.append(prepare_data)
web.run_app(app)
