import motor.motor_asyncio
from models import GitHubReposData


async def init_db(app):
    """
    Initialize connection to DB and inject to application
    :param app: application entity
    """
    conf = app['config']['mongo']
    engine = motor.motor_asyncio.AsyncIOMotorClient(
        conf['host'], conf['port']
    )
    app['db'] = engine[conf['database']][conf['collection']]


async def prepare_data(app):
    """
    Prepare data from GitHub. Could take some time
    :param app: application entity
    """
    data = GitHubReposData(app['config']['github'])
    data.fetch_github_data()
    app['github_data'] = data
