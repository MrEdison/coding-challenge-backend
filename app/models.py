import time

from github import Github
from github.GithubException import RateLimitExceededException


class GitHubReposData:
    """
    GitHub entity. Main object for this app. Store data fetched from github.
    """
    def __init__(self, config, language='python', fields=['full_name', 'html_url',
     'description', 'stargazers_count', 'language'], min_stars=5000):
        self._cache = []
        self._language = language
        self.github_entity = Github(config['user'], config['password'])
        self.fields = fields
        self._min_stars = min_stars


    def _fetch_github_data(self, repos):
        """
        Fetch data from given iterator.
        :param repos: iterator object with repositiries
        """
        try:
            for repo in repos:
                self._cache.append({i: getattr(repo, i) for i in self.fields})
                print({i: getattr(repo, i) for i in self.fields})
        except RateLimitExceededException:
            time.sleep(60)
            self._fetch_github_data(repos)

    def _get_repos_data(self, query):
        """
        Get iterator with all available repos based on search query.
        :param query: query for search API
        """
        repos = self.github_entity.search_repositories(query=query)
        self._fetch_github_data(repos)

    def _format_querry(self):
        """
        Format search query based on given params.
        """
        return 'language:{} stars:>={}'.format(self._language, self._min_stars)


    def fetch_github_data(self):
        self._get_repos_data(self._format_querry())

    @property
    def get_data(self):
        """
        Return cached data as a list.
        """
        return self._cache

    @property
    def get_data_count(self):
        """
        Return count of fetched object.
        """
        return len(self._cache)
