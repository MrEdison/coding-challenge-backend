async def do_insert(collection, data):
    """
    Insert given data to given collection.
    :param collection: collection object from app
    :param data: list of documents
    """
    await collection.insert_many(data)


def format_pagination(offset, limit, data_object, sort):
    """
    Format pagination for main endpoint.
    :param limit: int number of limitation
    :param offset: int number with offset
    :param data_object: GitHubReposData object
    :param sort: bool value
    """
    length = data_object.get_data_count
    if limit+offset > length:
        return {'message': 'Sorry, you reached limit'}
    data = data_object.get_data[offset:limit+offset]
    if sort:
        data = sorted(data, key=lambda k: k['stargazers_count'])

    return {'data': data,
            'next_page': '/?limit={}&offset={}'.format(
                min(limit, length-(limit+offset)), offset+limit
            ),
            'prev_page': '/?limit={}&offset={}'.format(
                limit, max(0, offset-limit)
            ),
            'available_count': length
            }
