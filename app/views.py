from aiohttp import web
from distutils.util import strtobool
from utils import do_insert, format_pagination


async def handle(request):
    """
    Main endpoint.
    :param request: request to server
    """
    limit = int(request.rel_url.query.get('limit', 10))
    offset = int(request.rel_url.query.get('offset', 0))
    sort = strtobool(request.rel_url.query.get('sort', 'false'))
    data = format_pagination(offset, limit, request.app['github_data'], sort)
    return web.json_response(data)


async def save_data(request):
    """
    Save data to db.
    :param request: request to server
    """
    collection = request.app['db']
    data = request.app['github_data'].get_data
    await do_insert(collection, data)
    return web.json_response({'message': 'Data saved to DB!'})
