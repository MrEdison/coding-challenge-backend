from views import handle, save_data


def setup_routes(app):
    """
    Setup routes fot app.
    """
    app.router.add_get('/', handle)
    app.router.add_get('/save', save_data)
